const path = require("path");
const fs = require("fs");
const babel = require("@babel/core");
const parser = require("@babel/parser");
const traverse = require("@babel/traverse").default;

module.exports = class Webpack {
  constructor(options) {
    this.entry = options.entry;
    this.output = options.output;
    this.modules = []; // init module
  }


  parse(entryFile) {
    const dependencies = {};
    const content = fs.readFileSync(entryFile, "utf-8");
    const ast = parser.parse(content, {
      sourceType: "module",
    }); // generate ast tree
    const dirname = path.dirname(entryFile);
    traverse(ast, {
      //visit
      ImportDeclaration(path1) {
        const { node } = path1;
        // for windows
        
        const path_import = path.join(dirname, node.source.value);
        dependencies[node.source.value] = path_import;
        // relativePath => absolutePath

      }
    });
    // es6 -> es5
    const {code} = babel.transformFromAst(ast, null, {
        presets: ["@babel/preset-env"]
    });

    return {
        code,
        dependencies,
        entryFile
    };
  }

  file(obj) {
      if(!fs.existsSync(this.output.path)) {
          fs.mkdirSync(this.output.path);
      }
    const filepath = path.join(this.output.path, this.output.filename);
    console.log('obj=>', obj);
    const newCode = JSON.stringify(obj);
    // generate bundle
    const bundle = `(function(graph){
            function otherDeps(relativePath) {
                return require(graph[module].dependencies[relativePath]);
            }
            function require(module){
                let exports = {};
                (function(require, exports, code){
                    eval(code);
                })(otherDeps, exports, graph[module].code);
            }
            require(path.resolve('${this.entry}'));
        })(${newCode})`;

    fs.writeFileSync(filepath, bundle, "utf-8");
  }
  run(){
      const result = this.parse(this.entry);
      this.modules.push(result)

        for(let module of this.modules){
            const {dependencies} = module;
            for(let j in dependencies){
                this.modules.push(this.parse(dependencies[j]));
            }
        }
        const obj = {};
        this.modules.forEach(item=>{
            obj[item.entryFile] = {
                dependencies: item.dependencies,
                code: item.code
            };
        });
        this.file(obj);

  }
};
