## mini-webpack

A mini webpack which can parse *.js file to es5 and can be understood by web browser.

1. Starting from entryFile, iterating every dependency with help of `(@babel/parser).parse` ImportDeclararion, and save realtivePath and aboluste Path.
2. Parse its code to es5 with the help  of babel/core and babel/traverse.
3. Generating bundle file. Because require / export browsers, will provide such method to browser.
```
const bundle = `(function(graph){
            function otherDeps(relativePath) {
                return require(graph[module].dependencies[relativePath]);
            }
            function require(module){
                let exports = {};
                (function(require, exports, code){
                    eval(code);
                })(otherDeps, exports, graph[module].code);
            }
            require(path.resolve('${this.entry}'));
        })(${newCode})`;
```
4. outsource bundle to output.
5. run **yarn test**
6. copy the dist/bundle.js to browser console, and run test

## future work
plugin
loader
