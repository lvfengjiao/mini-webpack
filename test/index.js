const Webpack  = require('../lib/webpack');
const options = require('./webpack.config');
const webpack = new Webpack(options);
webpack.run();